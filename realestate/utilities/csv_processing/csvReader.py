# Title        : csvReader file
# Purpose      : To create the necessary utilities to manipulate a CSV source
# Author       : Shanmugarajah Prasanna (SHPRLK)

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200414      SHPRLK              Added the csvToJson method which converts the csv to Json.
# 20200407      SHPRLK			    Created file.
# ----------------------------------------------------------------------------------------------------------------------

# Imports
import csv
import json


def csvToJson():
    jsonFilePath_ = (r'G:\Projects\Pycharm\Web\DjangoSetupTest\django_react\data_resources\train_short.csv')

    with open(jsonFilePath_) as f:
        a = [{k: (v) for k, v in row.items()}
             for row in csv.DictReader(f, skipinitialspace=True)]

    format_string_ = json.dumps(a, indent=2);
    return format_string_

csvToJson()