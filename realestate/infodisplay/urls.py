# Title        : urls file
# Purpose      : To Create the necessary mappings for the URLs to the information display path of the project
# Author       : Shanmugarajah Prasanna (SHPRLK)

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200414      SHPRLK              Added the index method which fetches the JSON of all data.
# 20200407      SHPRLK			    Created file.
# ----------------------------------------------------------------------------------------------------------------------

# Imports

from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]