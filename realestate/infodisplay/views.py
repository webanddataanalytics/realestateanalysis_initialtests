# Title        : Views file for information display
# Purpose      : To create views for the information to be displayed
# Author       : Shanmugarajah Prasanna (SHPRLK)

# DATE         CONTRIBUTOR ID       COMMENT
#---------     --------------      ------------------------------------------------------------------------------------
# 20200414      SHPRLK              Added the index method which fetches the JSON of all data.
# 20200407      SHPRLK			    Created file.
#----------------------------------------------------------------------------------------------------------------------

# imports
from django.http import HttpResponse
import sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from utilities.csv_processing import csvReader                  # Pointing at the utilities which help convert csv to JSON


# Mapping
def index(request):
    return HttpResponse(csvReader.csvToJson())

